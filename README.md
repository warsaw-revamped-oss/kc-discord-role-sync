Discord <-> Keycloak role sync bot
==================================

This bot synchronises [Keycloak](https://www.keycloak.org/) groups to Discord roles, making Keycloak the source of truth for permissions, in the fastest and most minimal way possible.

Building
--------
This is a pure `cargo` project, see [the Cargo book](https://doc.rust-lang.org/cargo/getting-started/first-steps.html).  
TL;DR: `cargo build --release` to build a release binary, `cargo run` to run a development build.

Bot configuration
-----------------
The bot is configured using environment variables (see [src/config.rs](src/config.rs)).  
It will also load environment variables from a `.env` file placed in the working directory.

The following options are available:

| Variable            | Required | Example value                                           | Description                                                                                                                                                                                           |
|---------------------|----------|---------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `BOT_TOKEN`         | &#9745;  | `abcdefg.xyz`                                           | Discord bot token. Create one on [Discord](https://discord.com/developers/applications). The bot will need at least "Manage roles" permissions and should be placed at the top of the role hierarchy. |
| `KEYCLOAK_HOST`     | &#9745;  | `https://id.domain.com`                                 | Base URL of your Keycloak instance                                                                                                                                                                    |
| `KEYCLOAK_REALM`    | &#9745;  | `myrealm`                                               | Name of the Keycloak realm you want to sync with                                                                                                                                                      |
| `KEYCLOAK_USERNAME` | &#9745;  | `discord-bot`                                           | Username to use for authentication with the Keycloak admin API. Needs permissions to view groups and users in your realm.                                                                             |
| `KEYCLOAK_PASSWORD` | &#9745;  | `secretsauce`                                           | Password for the Keycloak user.                                                                                                                                                                       |
| `GUILDS`            | &#9745;  | `public:469622292818728980;internal:722972618198698275` | A mapping of Discord server alias (chosen by you) to guild ID that the bot should synchronise.                                                                                                        |
| `IGNORE_ROLES`      | &#9744;  | `469611687274341632,906205960183752476`                 | A list of Discord role IDs to always ignore during synchronisation. If a Discord member has these roles, they will be kept intact.                                                                    |
| `SYNC_INTERVAL`     | &#9744;  | `300` (Default)                                         | How often the synchronisation should take place, in seconds.                                                                                                                                          |
| `RUST_LOG`          | &#9744;  | `INFO` (Default)                                        | Log level. See [Level](https://docs.rs/tracing/latest/tracing/struct.Level.html).                                                                                                                     |

Usage with Keycloak
-------------------
Your Keycloak instance needs to run the [Discord identity provider](https://github.com/wadahiro/keycloak-discord) and
your users have to connect their Discord accounts in the federated identity section of their account settings.

On each Keycloak group that you want to associate with a Discord role, add an _Attribute_ in the following format:

| Key                                           | Value         |
|-----------------------------------------------|---------------|
| `discord_id_{guild_alias}_{optional_postfix}` | `{role_id}`   |

with `guild_tag` being the alias for the Discord guild you configured with the `GUILDS` environment variable, and `role_id` being a Discord role ID on that Discord guild.  
`optional_postfix` can be appended for de-duplication purposes (so you can define more than one role per Keycloak group) and is not used by the bot.

Example:

| Key                         | Value                 |
|-----------------------------|-----------------------|
| `discord_id_public`         | `1004848059395612823` |
| `discord_id_public_2ndrole` | `1004848059395423231` |


Attributes applied to a parent group will be passed down and given to any member in any subgroup of that parent group too.


Running
-------
Simply run the built executable with the previously mentioned environment variables defined. You can also create a `.env` file in the working directory which will be read by the bot.

**_Warning_**: The bot will **remove all roles** from all users that are not defined through Keycloak.  
If you are unsure about your configuration, it is advisable to run the bot with the `--dry-run` flag first, which will skip making any changes.