use crate::discord::{create_client, get_all_guild_members, synchronise_roles};
use clap::Parser;
use dotenv::dotenv;
use envconfig::Envconfig;
use futures::future::join_all;
use serenity::model::guild::Member;
use serenity::model::id::RoleId;
use serenity::prelude::SerenityError;
use std::collections::HashSet;
use std::sync::Arc;
use tokio::task::JoinHandle;
use tokio::time::Instant;
use tokio::time::{sleep, Duration};
use tracing::{debug, error, info};
use tracing_subscriber::filter::LevelFilter;
use tracing_subscriber::{EnvFilter, FmtSubscriber};

mod config;
mod discord;
mod keycloak_users;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    #[clap(short, long)]
    /// Only check for changes and do not actually synchronise them
    dry_run: bool,
}

#[derive(Default)]
struct SyncResults {
    succeeded: usize,
    failed: usize,
}

#[tokio::main]
async fn main() {
    // Set environment variables from .env file if present
    dotenv().ok();

    // Parse args
    let args = Args::parse();

    FmtSubscriber::builder()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .init();

    // Load config
    let conf: Arc<config::Config> = Arc::new(config::Config::init_from_env().unwrap());

    // Create Discord bot
    let dc = create_client(conf.clone()).await.unwrap();

    info!("Configured guilds: {:?}", conf.guilds);

    let mut first_run = true;

    loop {
        if !first_run {
            info!("Next run in {}s", conf.sync_interval);
            sleep(Duration::from_secs(conf.sync_interval)).await;
        }
        first_run = false;
        if args.dry_run {
            info!("Checking for changes (dry run)");
        } else {
            info!("Synchronising");
        }
        let start = Instant::now();

        // Get relevant Keycloak users
        let users = match keycloak_users::get_users(conf.clone()).await {
            Ok(users) => Arc::new(users),
            Err(e) => {
                error!("Failed to retrieve Keycloak users: {}", e);
                continue;
            }
        };
        info!("Found {} relevant users: {:?}", users.len(), users.values());

        // Iterate configured guilds and fetch guild members
        for (guild_tag, guild_id) in conf.guilds.0.iter() {
            // The ideal procedure would be:
            // 1. Find all roles and all members of all roles (basically anyone who has a specific role)
            // 2. Find all members based on the fetched users
            // 3. Sync roles for members of both of these groups (in parallel)
            // Unfortunately this does not seem possible as Discord does not offer an API to get members
            // that have a specific role assigned, so we are stuck with this:

            // Find ALL guild members
            debug!("[{}] Fetching guild members", guild_tag);
            let guild_members =
                match get_all_guild_members(dc.cache_and_http.http.clone(), *guild_id).await {
                    // Filter out bots
                    Ok(guild_members) => guild_members
                        .into_iter()
                        .filter(|member| !member.user.bot)
                        .collect::<Vec<Member>>(),
                    Err(e) => {
                        error!("[{}] Failed to retrieve members: {}", guild_tag, e);
                        continue;
                    }
                };

            info!(
                "[{}] Found {} members (excluding bots)",
                guild_tag,
                guild_members.len()
            );

            // Fetch all guild roles
            debug!("[{}] Fetching guild roles", guild_tag);
            let guild_roles = match dc.cache_and_http.http.get_guild_roles(*guild_id).await {
                Ok(guild_roles) => guild_roles,
                Err(e) => {
                    error!("[{}] Failed to retrieve roles: {}", guild_tag, e);
                    continue;
                }
            };
            debug!("[{}] Guild roles: {:?}", guild_tag, guild_roles);

            // Filter guild roles - these roles will be ignored/left alone on sync because they
            // are special in some way, managed by integrations, guild premium sub, etc. or configured as such
            // Collected to HashSet for difference operations
            let ignored_roles = guild_roles
                .iter()
                .filter(|guild_role| {
                    guild_role.managed
                        || guild_role.tags.bot_id != None
                        || guild_role.tags.integration_id != None
                        || guild_role.tags.premium_subscriber
                        || Some(true)
                            == conf
                                .ignore_roles
                                .clone()
                                .map(|conf_ignored| conf_ignored.0.contains(&guild_role.id.0))
                })
                .map(|role| role.id.0)
                .collect::<HashSet<u64>>();

            info!(
                "[{}] Ignoring {} managed or explicitly ignored roles: {:?}",
                guild_tag,
                ignored_roles.len(),
                ignored_roles
            );

            // Iterate guild members and create sync tasks for all of them
            let sync_results = join_all(
                guild_members
                    .iter()
                    .filter_map(|member| {
                        let member_id = member.user.id.0;
                        let http = dc.cache_and_http.http.clone();
                        let users = users.clone();
                        let guild_id = guild_id.clone();

                        // Find expected roles from previously fetched users map and collect to hashset for difference operations
                        let mut expected_roles = users
                            .get(&member_id)
                            .map(|user| user.discord_roles_by_guild.get(&guild_id))
                            .flatten()
                            .unwrap_or(&HashSet::new())
                            .iter()
                            .map(|role_id| *role_id)
                            .collect::<HashSet<u64>>();

                        // Collect member roles to hashset for difference operations
                        let member_roles = member
                            .roles
                            .iter()
                            .map(|role_id| role_id.0)
                            .collect::<HashSet<u64>>();

                        // Determine roles that this member has that we want to leave alone
                        let ignored_member_roles = member_roles
                            .intersection(&ignored_roles)
                            .cloned()
                            .collect::<HashSet<u64>>();

                        debug!(
                            "[{}] Ignoring roles for member: {} ({}): {:?}",
                            guild_tag,
                            member_id,
                            member.user.tag(),
                            ignored_member_roles
                        );

                        // Add ignored member roles to the expected ones - we want to keep them around
                        expected_roles = expected_roles
                            .union(&ignored_member_roles)
                            .cloned()
                            .collect::<HashSet<u64>>();

                        let added_roles = expected_roles
                            .difference(&member_roles)
                            .collect::<Vec<&u64>>();
                        let removed_roles = member_roles
                            .difference(&expected_roles)
                            .collect::<Vec<&u64>>();

                        if added_roles.len() > 0 {
                            info!(
                                "[{}] Roles added for member: {} ({}): {:?}",
                                guild_tag,
                                member_id,
                                member.user.tag(),
                                added_roles
                            );
                        }
                        if removed_roles.len() > 0 {
                            info!(
                                "[{}] Roles removed for member: {} ({}): {:?}",
                                guild_tag,
                                member_id,
                                member.user.tag(),
                                removed_roles
                            );
                        }

                        if added_roles.len() > 0 || removed_roles.len() > 0 {
                            if args.dry_run {
                                debug!(
                                    "[{}] Would set expected roles for member: {} ({}): {:?}",
                                    guild_tag,
                                    member_id,
                                    member.user.tag(),
                                    expected_roles
                                );
                                return None;
                            }
                            info!(
                                "[{}] Setting expected roles for member: {} ({}): {:?}",
                                guild_tag,
                                member_id,
                                member.user.tag(),
                                expected_roles
                            );
                            Some(tokio::spawn(async move {
                                (
                                    member_id,
                                    synchronise_roles(
                                        http,
                                        guild_id,
                                        member_id,
                                        expected_roles.into_iter().map(|role_id| RoleId(role_id)),
                                    )
                                    .await,
                                )
                            }))
                        } else {
                            debug!(
                                "[{}] Roles unchanged for member: {} ({}), skipping",
                                guild_tag,
                                member_id,
                                member.user.tag()
                            );
                            None
                        }
                    })
                    .collect::<Vec<JoinHandle<(u64, Result<(), SerenityError>)>>>(),
            )
            .await
            .into_iter()
            .filter_map(|task| task.ok())
            .fold(SyncResults::default(), |mut acc, (member_id, result)| {
                match result {
                    Ok(()) => acc.succeeded += 1,
                    Err(e) => {
                        error!(
                            "[{}] Failed to sync roles for member: {}: {}",
                            guild_tag, member_id, e
                        );
                        acc.failed += 1
                    }
                }
                acc
            });
            if !args.dry_run {
                info!(
                    "[{}] Successfully synced {} users",
                    guild_tag, sync_results.succeeded
                );
                if sync_results.failed > 0 {
                    error!(
                        "[{}] Failed syncing {} users",
                        guild_tag, sync_results.failed
                    );
                }
            }
        }
        if args.dry_run {
            info!(
                "Dry run completed in {}s",
                (Instant::now() - start).as_secs()
            );
        } else {
            info!(
                "Synchronisation completed in {}s",
                (Instant::now() - start).as_secs()
            );
        }
    }
}
