use crate::config::Config;
use futures::future::join_all;
use itertools::Itertools;
use keycloak::{
    types::*,
    KeycloakError, {KeycloakAdmin, KeycloakAdminToken},
};
use regex::Regex;
use serenity::json::Value;
use std::collections::{HashMap, HashSet};
use std::str::FromStr;
use std::sync::Arc;
use tracing::{debug, info, warn};

const ROLE_ATTRIBUTE_REGEX: &str = r"^discord_id_(?P<tag>[a-zA-Z\d]+)(?:_[a-zA-Z\d]+)?$";

/// Describes a Keycloak user complete with their Discord user ID and a list of Discord roles by guild ID
#[derive(Clone, Debug)]
pub struct UserWithRoles {
    discord_id: u64,
    pub discord_roles_by_guild: HashMap<u64, HashSet<u64>>,
}

/// Describes a Keycloak group with the Discord role ID associated with each Discord guild ID
#[derive(Clone, Debug)]
pub struct GroupWithRoles {
    keycloak_id: String,
    discord_roles_by_guild: HashMap<u64, HashSet<u64>>,
}

#[derive(Clone, Debug)]
pub struct GroupWithMembers {
    group: GroupWithRoles,
    members: Vec<UserRepresentation>,
}

///// TDOO ACTUALLY mostly go back to the old thing but don't do it stupidly and keep track of the parent roles in the recursion, no need to already get users here

fn unwrap_groups(
    config: Arc<Config>,
    groups: Vec<GroupRepresentation>,
    parent_roles: Option<HashMap<u64, HashSet<u64>>>,
) -> Vec<GroupWithRoles> {
    let role_attribute_regex = Regex::new(ROLE_ATTRIBUTE_REGEX).unwrap();

    let mut groups_with_roles: Vec<GroupWithRoles> = Vec::new();

    for group in groups {
        // Parse roles per guild for this group and add them to the current tracked parent groups for the later recursive call
        let mut group_roles: HashMap<u64, HashSet<u64>> = match parent_roles {
            Some(ref roles) => roles.clone(),
            None => HashMap::new(),
        };

        if let Some(attributes) = group.attributes {
            for (k, v) in attributes.into_iter() {
                // Extract guild tag from attribute if applicable
                if let Some(guild_tag) = role_attribute_regex
                    .captures(&k)
                    .map(|captures| captures.name("tag").map(|group| group.as_str().to_owned()))
                    .flatten()
                {
                    // Resolve guild ID from config if any
                    if let Some(guild_id) = config.guilds.0.get(&guild_tag) {
                        // Parse attribute value as role ID
                        if let Value::Array(id_list) = v {
                            if let Some(Value::String(val)) = id_list.first() {
                                if let Some(role_id) = u64::from_str(val).ok() {
                                    // Update both HashMaps
                                    group_roles
                                        .entry(*guild_id)
                                        .or_insert_with(|| HashSet::new())
                                        .insert(role_id);
                                }
                            } else {
                                warn!(
                                    "Group {:?} has no attribute value for Discord guild '{}'",
                                    group.name, guild_tag
                                );
                            }
                            if id_list.len() > 1 {
                                warn!("Group {:?} has duplicate attributes for Discord guild '{}', using the first one", group.name, guild_tag);
                            }
                        }
                    }
                }
            }
        }

        if let Some(keycloak_id) = group.id {
            groups_with_roles.push(GroupWithRoles {
                keycloak_id,
                discord_roles_by_guild: group_roles.clone(),
            })
        }

        // Recursively call for subgroups
        if let Some(sub_groups) = group.sub_groups {
            groups_with_roles.append(&mut unwrap_groups(
                config.clone(),
                sub_groups,
                Some(group_roles),
            ));
        }
    }

    groups_with_roles
}

async fn get_user_identity(
    config: &Config,
    admin: &KeycloakAdmin,
    keycloak_id: &str,
) -> Result<Option<UserWithRoles>, KeycloakError> {
    // Get federated identity and insert user if one exists
    if let Some(discord_identity) = admin
        .realm_users_with_id_federated_identity_get(&config.keycloak_realm, keycloak_id)
        .await?
        .iter()
        .find(|identity| identity.identity_provider == Some("discord".to_string()))
    {
        return if let Some(discord_id) = discord_identity
            .user_id
            .as_ref()
            .map(|id| u64::from_str(&id).ok())
            .flatten()
        {
            Ok(Some(UserWithRoles {
                discord_id,
                discord_roles_by_guild: Default::default(),
            }))
        } else {
            debug!(
                "User {} has has no valid Discord ID: {:?}",
                keycloak_id, discord_identity
            );
            Ok(None)
        };
    }
    debug!(
        "User {} does not have a Discord federated identity",
        keycloak_id
    );
    Ok(None)
}

/// Get all users from Keycloak that are expected to have a specific Discord role
pub async fn get_users(config: Arc<Config>) -> Result<HashMap<u64, UserWithRoles>, KeycloakError> {
    // Create Keycloak API client (we don't keep it around for resilience)
    let client = reqwest::Client::new();
    let admin_token = KeycloakAdminToken::acquire_custom_realm(
        &config.keycloak_host,
        &config.keycloak_username,
        &config.keycloak_password,
        &config.keycloak_realm,
        "admin-cli",
        "password",
        &client,
    )
    .await?;
    let admin = Arc::new(KeycloakAdmin::new(
        &config.keycloak_host,
        admin_token,
        client,
    ));

    // Fetch all groups
    let groups = unwrap_groups(
        config.clone(),
        admin
            .realm_groups_get(&config.keycloak_realm, Some(false), None, None, None)
            .await?,
        None,
    );

    info!("Found {} relevant groups: {:?}", groups.len(), groups);

    // Fetch members for all groups in question in parallel
    let groups_with_members = join_all(groups.into_iter().map(|group| {
        let config = config.clone();
        let admin = admin.clone();
        tokio::spawn(async move {
            let members = admin
                .realm_groups_with_id_members_get(
                    &config.keycloak_realm,
                    &group.keycloak_id,
                    Some(true), // we only really need the IDs so brief repr is enough
                    None,
                    None,
                )
                .await?;
            Ok(GroupWithMembers { group, members })
        })
    }))
    .await
    .into_iter()
    .filter_map(|task| task.ok())
    .collect::<Result<Vec<GroupWithMembers>, KeycloakError>>()?;

    debug!("Groups with members: {:?}", groups_with_members);

    // Create a unique list of members and fetch their federated identities in parallel
    let federated_identities = join_all(
        groups_with_members
            .iter()
            .map(|g| g.members.iter().map(|m| m.id.clone()))
            .flatten()
            .flatten()
            .unique()
            .map(|keycloak_id| {
                let config = config.clone();
                let admin = admin.clone();
                let keycloak_id = keycloak_id.clone();
                tokio::spawn(async move {
                    Ok((
                        keycloak_id.clone(),
                        get_user_identity(&config, &admin, &keycloak_id).await?,
                    ))
                })
            }),
    )
    .await
    .into_iter()
    .filter_map(|task| task.ok())
    .filter_map(|result| match result {
        Ok((keycloak_id, identity_opt)) => match identity_opt {
            Some(identity) => Some(Ok((keycloak_id, identity))),
            None => None,
        },
        Err(e) => Some(Err(e)),
    })
    .collect::<Result<HashMap<String, UserWithRoles>, KeycloakError>>()?;

    // Make a flat hash map of users by Discord ID to their roles per guild
    let mut users: HashMap<u64, UserWithRoles> = HashMap::new();
    for group in groups_with_members {
        for group_member in group.members {
            if let Some(member_id) = group_member.id {
                // Try to find federated identity for the user
                if let Some(federated_identity) = federated_identities.get(&member_id) {
                    // Get or create user
                    let user = users
                        .entry(federated_identity.discord_id)
                        .or_insert(federated_identity.clone());
                    // Append roles
                    for (guild_id, role_ids) in group.group.discord_roles_by_guild.iter() {
                        let role_list = user
                            .discord_roles_by_guild
                            .entry(*guild_id)
                            .or_insert(Default::default());
                        role_list.extend(role_ids)
                    }
                }
            }
        }
    }

    Ok(users)
}
