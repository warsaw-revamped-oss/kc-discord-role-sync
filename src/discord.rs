use crate::config::Config;
use serenity::builder::EditMember;
use serenity::constants::MEMBER_FETCH_LIMIT;
use serenity::http::Http;
use serenity::model::guild::Member;
use serenity::model::id::RoleId;
use serenity::prelude::{GatewayIntents, SerenityError};
use serenity::{json, Client};
use std::sync::Arc;
use tracing::debug;

/**
Create the Discord client

# Arguments
* `conf` - The configuration to use
**/
pub async fn create_client(conf: Arc<Config>) -> serenity::Result<Client> {
    Client::builder(&conf.bot_token, GatewayIntents::empty()).await
}

/**
Fetch all members by pagination

# Arguments
* `http` - The serenity-rs http API client
* `guild_id` - The guild to fetch members for
**/
pub async fn get_all_guild_members(
    http: Arc<Http>,
    guild_id: u64,
) -> Result<Vec<Member>, SerenityError> {
    let mut members: Vec<Member> = Vec::new();
    loop {
        // Get guild members starting with the last one in the current list
        let mut current = http
            .get_guild_members(
                guild_id,
                Some(MEMBER_FETCH_LIMIT),
                members.last().map(|member| member.user.id.0),
            )
            .await?;
        let count = current.len();
        members.append(&mut current);
        // Break if the response is not at the limit (no more members can be fetched)
        if (count as u64) < MEMBER_FETCH_LIMIT {
            break;
        }
    }
    Ok(members)
}

pub async fn synchronise_roles(
    http: Arc<Http>,
    guild_id: u64,
    user_id: u64,
    expected_roles: impl Iterator<Item = RoleId>,
) -> Result<(), SerenityError> {
    debug!("[{}] Synchronising roles for member: {}", guild_id, user_id);

    let mut builder = EditMember::default();
    builder.roles(expected_roles);
    let map = json::hashmap_to_json_map(builder.0);

    http.edit_member(guild_id, user_id, &map, None).await?;
    Ok(())
}
