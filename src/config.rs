use envconfig::Envconfig;
use std::cmp::Eq;
use std::collections::HashMap;
use std::fmt::Debug;
use std::hash::Hash;
use std::str::FromStr;
use thiserror::Error;

#[derive(Debug, Clone)]
pub struct EnvList<T: FromStr>(pub Vec<T>);

impl<T: FromStr> FromStr for EnvList<T> {
    type Err = <T as FromStr>::Err;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(EnvList(
            s.split(',')
                .map(|x| T::from_str(x))
                .collect::<Result<Vec<T>, Self::Err>>()?,
        ))
    }
}

#[derive(Debug, Clone)]
pub struct EnvHashMap<A: FromStr, B: FromStr>(pub HashMap<A, B>);

#[derive(Error, Debug)]
pub enum EnvHashMapParseError<K: FromStr, V: FromStr>
where
    <K as FromStr>::Err: Debug,
    <V as FromStr>::Err: Debug,
{
    #[error("List parse failed")]
    List(<EnvList<String> as FromStr>::Err),
    #[error("Map item has an invalid amount of items")]
    MapItem,
    #[error("Map key parse failed")]
    MapKey(<K as FromStr>::Err),
    #[error("Map value parse failed")]
    MapValue(<V as FromStr>::Err),
}

impl<K: FromStr + Hash + Eq, V: FromStr + Hash + Eq> FromStr for EnvHashMap<K, V>
where
    <K as FromStr>::Err: Debug,
    <V as FromStr>::Err: Debug,
{
    type Err = EnvHashMapParseError<K, V>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(EnvHashMap(
            EnvList::<String>::from_str(s)
                .map_err(|e| EnvHashMapParseError::List(e))?
                .0
                .iter()
                .map(|x| {
                    let mut split = x.split(':');
                    Ok((
                        split
                            .next()
                            .ok_or(EnvHashMapParseError::MapItem)
                            .and_then(|val| {
                                K::from_str(val).map_err(|e| EnvHashMapParseError::MapKey(e))
                            })?,
                        split
                            .next()
                            .ok_or(EnvHashMapParseError::MapItem)
                            .and_then(|val| {
                                V::from_str(val).map_err(|e| EnvHashMapParseError::MapValue(e))
                            })?,
                    ))
                })
                .collect::<Result<HashMap<K, V>, EnvHashMapParseError<K, V>>>()?,
        ))
    }
}

#[derive(Envconfig, Debug)]
pub struct Config {
    #[envconfig(from = "BOT_TOKEN")]
    pub bot_token: String,
    #[envconfig(from = "KEYCLOAK_HOST")]
    pub keycloak_host: String,
    #[envconfig(from = "KEYCLOAK_REALM")]
    pub keycloak_realm: String,
    #[envconfig(from = "KEYCLOAK_USERNAME")]
    pub keycloak_username: String,
    #[envconfig(from = "KEYCLOAK_PASSWORD")]
    pub keycloak_password: String,
    #[envconfig(from = "SYNC_INTERVAL", default = "300")]
    pub sync_interval: u64,
    /// Guilds are a mapping of human readable name to guild ID
    #[envconfig(from = "GUILDS")]
    pub guilds: EnvHashMap<String, u64>,
    /// Discord role IDs to ignore in synchronisation
    #[envconfig(from = "IGNORE_ROLES")]
    pub ignore_roles: Option<EnvList<u64>>,
}
